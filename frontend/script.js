function getRadioValue() {
	let buttons = document.getElementsByName('choice');

	let result = null

	for (i = 0; i < buttons.length; i++) {
		if (buttons[i].checked) result = buttons[i].value
	}

	return result
}

function changeFeedbackText(newText) {
	document.getElementById('feedback').innerText = newText
}

function sendChoice(event) {
	event.preventDefault()

	let nameValue = document.getElementById("username").value;
	let choiceValue = getRadioValue()

	if (!choiceValue || !nameValue) {
		changeFeedbackText("Choose your name/option")
		return
	}

	const url = "http://35.193.50.241:3100/play"
	const messageBody = {
		username: nameValue,
		choice: choiceValue
	}

	const options = {
		method: 'POST',
		body: JSON.stringify(messageBody),
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		}
	}


	fetch(url, options)
		.then(response => response.text())
		.then(text => changeFeedbackText(text))
		.then(() => updateUserInfo())
}

async function fetchUserInfo() {
	let nameValue = document.getElementById("username").value;
	const url = "http://35.193.50.241:3100/user"
	const messageBody = {
		username: nameValue
	}

	const options = {
		method: 'POST',
		body: JSON.stringify(messageBody),
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		}
	}

	const userObject = await fetch(url, options)
		.then(response => response.text())
		.then(response => JSON.parse(response))

	return userObject
}

async function updateUserInfo() {
	const userObject = await fetchUserInfo()

	let scoreDisplay = document.getElementById('infoScore');
	scoreDisplay.innerHTML = userObject.score

	let victoriesDisplay = document.getElementById('infoVictories');
	victoriesDisplay.innerHTML = userObject.victories

	let matchesPlayedDisplay = document.getElementById('infoMatchesPlayed')
	matchesPlayedDisplay.innerHTML = userObject.matchesPlayed

	let currentStreakDisplay = document.getElementById('infoCurrentStreak')
	currentStreakDisplay.innerHTML = userObject.currentStreak

	let bestStreakDisplay = document.getElementById('infoBestStreak')
	bestStreakDisplay.innerHTML = userObject.bestStreak

}