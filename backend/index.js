const express = require('express')
const bp = require('body-parser')
const app = express()
const port = 3100

app.use(bp.json())
app.use(bp.urlencoded({ extended: true }))

const cors = require('cors')
app.use(cors())

//name,wins,plays,current_streak, best_streak
const users = []

function getTopScores() {
	const usersSorted = users.slice(0).sort((a, b) => (a.score > b.score) ? 1 : -1)
	return usersSorted.slice(0, 5)
}

function getTopStreaks() {
	const usersSorted = users.slice(0).sort((a, b) => (a.bestStreak > b.bestStreak) ? 1 : -1)
	return usersSorted.slice(0, 5)
}

function isUserWinner(userChoice, pcChoice) {
	return ((userChoice === 2 && pcChoice === 1)
		|| (userChoice === 1 && pcChoice === 0)
		|| (userChoice === 0 && pcChoice === 2))
}

function evaluateMatch(userChoice, pcChoice) {
	if (userChoice === pcChoice) return 0
	//rock=0, paper=1, scissor=2
	if (isUserWinner(userChoice, pcChoice)) {
		return 1
	} else {
		return -1
	}
}

function codeToTextChoice(code) {
	if (code === 0) return "rock"
	if (code === 1) return "paper"
	if (code === 2) return "scissors"
}

function codeToTextResult(code) {
	if (code === 1) return "victory"
	if (code === 0) return "draw"
	if (code === -1) return "defeat"
}

module.exports = { codeToTextChoice, codeToTextResult };

function getFeedbackText(userChoice, pcChoice, result) {
	const feedbackText = `You: ${codeToTextChoice(userChoice)} 
Opponent: ${codeToTextChoice(pcChoice)} 
Result: ${codeToTextResult(result)}`
	return feedbackText
}

function isUserRegistered(username) {
	let isFound = false

	users.forEach(user => {
		if (user.username == username) {
			isFound = true
		}
	})

	return isFound
}

function updateUserScore(username, result) {
	for (const user of users) {
		if (user.username === username) {

			user.matchesPlayed += 1
			user.score += result

			if (result === 1) {
				user.victories += 1
				user.currentStreak += 1

				if (user.currentStreak > user.bestStreak) user.bestStreak = user.currentStreak
			}

			if (result === -1) {
				user.currentStreak = 0
			}

			break;
		}
	}
}

function updateRecord(username, result) {
	if (!isUserRegistered(username)) createNewUser(username)
	updateUserScore(username, result)
}

function chooseRandom() {
	const random = Math.floor(Math.random() * 3);
	return random
}

function play(username, userChoice) {
	const pcChoice = chooseRandom()

	const result = evaluateMatch(userChoice, pcChoice);

	updateRecord(username, result);

	const feedbackText = getFeedbackText(userChoice, pcChoice, result)

	return feedbackText
}



function createNewUser(username) {
	const user = {
		username: username,
		score: 0,
		victories: 0,
		matchesPlayed: 0,
		currentStreak: 0,
		bestStreak: 0
	}

	users.push(user)
}

function getUser(username) {
	let result = null
	users.forEach(user => {
		if (user.username === username) result = user
	})
	return result
}


app.get('/', (req, res) => {
	res.send('Hello World!')
})

app.get('/topscores', (req, res) => {
	const topScores = getTopScores()
	res.send(JSON.stringify(topScores))
})

app.get('/topstreaks', (req, res) => {
	const topStreaks = getTopStreaks
	res.send(JSON.stringify(topStreaks))
})

app.post('/newuser', (req, res) => {
	const username = req.body.username
	users.push(username)
	res.send('adicionei: ' + username)
})

app.get('/users', (req, res) => {
	res.send(users)
})

app.post('/user', (req, res) => {
	const username = req.body.username
	const user = getUser(username)
	res.send(user)
})

app.post('/play', (req, res) => {
	const username = req.body.username
	//rock=0, paper=1, scissor=2
	const userChoice = Number(req.body.choice)

	const feedbackText = play(username, userChoice)

	res.send(feedbackText)
})


app.listen(port, () => {
	console.log(`rock-paper-scissor server listening at http://localhost:${port}`)
})