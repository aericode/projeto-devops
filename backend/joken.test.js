const { codeToTextChoice, codeToTextResult } = require('./index');

test('Expects paper', () => expect(codeToTextChoice(1)).toBe('paper'));
test('Expects rock', () => expect(codeToTextChoice(0)).toBe('rock'));
test('Expects scissors', () => expect(codeToTextChoice(2)).toBe('scissors'));

test('Expects defeat', () => expect(codeToTextResult(-1)).toBe('defeat'));
test('Expects draw', () => expect(codeToTextResult(0)).toBe('draw'));
test('Expects victory', () => expect(codeToTextResult(1)).toBe('victory'));